![picture](https://i.imgur.com/GyfFGVa.png)

Unofficial Google Keep application, Open source and multi-platform for all platforms to use.


Get access to all your note, reminders and more just like on the web version all wrapped up into a desktop application!

![picture](https://9to5google.com/wp-content/uploads/sites/4/2019/07/Google-Keep-dark-mode-web.jpg)

![picture](https://www.cnet.com/a/img/35MFrw0tpuHzvwmZrwAGYbEd5yY=/940x0/2019/07/09/c9ede8c4-d4a6-42ab-acb1-8623ed95e36d/keepweb.png)


&nbsp;&nbsp;&nbsp;&nbsp;

 You can install Youtube from the AUR for Arch/Manjaro distros.
 [Click Here](https://aur.archlinux.org/packages/googlekeep/)

 ### Download For All platforms (Linux, Mac OS and Windows)
  
  [Click to get the latest release](https://gitlab.com/googlekeep-desktop/application/-/releases)

 ### Author
  * Corey Bruce
